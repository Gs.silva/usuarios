﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace Usuarios
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://senacao.tk/objetos/usuario";
            Usuario usuario = BuscarUsuario(url);
            Console.WriteLine("Usuario");
            Console.WriteLine(
                String.Format("Nome: {0} \n Email: {1} \n Telefone: {2} \n Endereco {3}",
                usuario.Nome, usuario.Email, usuario.Telefone, usuario.Endereco.Cidade, usuario.Endereco.Bairro, 
                usuario.Endereco.Rua, usuario.Endereco.Numero, usuario.Endereco.Uf));
            Console.WriteLine("\n\n");
            Console.WriteLine("Conhecimentos");   
            

            foreach(string Conhecimentos in usuario.Conhecimentos)
            {
                Console.WriteLine(Conhecimentos);
            }

            Console.WriteLine("\n\n");
            foreach (Qualificacao qualificacao in usuario.Qualificacoes)
            {
                Console.WriteLine(qualificacao.Nome);
                Console.WriteLine(qualificacao.Instituicao);
                Console.WriteLine(qualificacao.Ano);
            }
            Console.ReadLine();
        }
       

        public static Usuario BuscarUsuario(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(content);
            return usuario;
        }



    }
}
