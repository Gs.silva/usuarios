﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Usuarios
{
    class Usuario
    {
        public string Nome { get; set; }
        
        public string Email { get; set; }
        public string Telefone { get; set; }
        public Endereco Endereco { get; set; }
        public List<string> Conhecimentos { get; set; } 
        public List<Qualificacao> Qualificacoes { get; set; }

    }
    class Endereco
    {
        public string Rua { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Uf { get; set; }
    }



    internal class Qualificacao
    {
        public string Nome { get; set; }
        public string Instituicao { get; set; }
        public int Ano{ get; set; }


    }
}
